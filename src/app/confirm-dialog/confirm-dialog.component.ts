import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite/ngx';
import { ModalController } from '@ionic/angular';
 
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent implements OnInit {
  @Input() email: string;
  @Input() password: string;
 
  
  constructor(private sqlite: SQLite, private router: Router, private modalCtrl:ModalController) { } 
   
  ngOnInit() {}
  SaveUserdetails(){
      var s =this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
       let script ="insert into UserDetails(username,password ) values('"+this.email+"','"+this.password+"');";
       db.executeSql("create table IF NOT EXISTS UserDetails(username varchar(20),password varchar(20)) ");
       db.transaction(tx=>tx.executeSql(script));
       console.log('details saved');
       this.dismiss();
       this.router.navigate(['dashboard']);
       
      });
          
    }
  dismiss() {
    
    this.router.navigate(['dashboard']);
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
