import { TestBed } from '@angular/core/testing';

import { EventPassService } from './event-pass.service';

describe('EventPassService', () => {
  let service: EventPassService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventPassService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
