import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'numbersPipe'
})
export class GetArrayOfNumbers implements PipeTransform {
    transform(num) {
        return Array(num);
    }
}