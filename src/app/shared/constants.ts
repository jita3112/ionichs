// import { environment } from 'src/environments/environment.prod'
import { environment } from './../../environments/environment';

//Carried over from healthSlice App


export class Constants {
  httpBaseUrl: string = environment.baseUrl;
  surveyController: string = "Survey/";
  accountController: string = "Accounts/";
  loginController: string = "Auth/";
  getFavCards: string = "/favcards"; //dummy
  getAllCards: string = "/mycards"; //dummy
  getValidations: string = "Validation/";
  surveyViewController: string = "Survey";
  surveyResponseController: string = this.httpBaseUrl + "SurveyResponse/";
  surveyResultController: string = this.httpBaseUrl + "SurveyResult/";
  validationController: string = this.httpBaseUrl + "validation/";
  userManagementController: string = this.httpBaseUrl + "UserManagment/";
  validationdelController: string = this.httpBaseUrl + "Validation";
  errorLogController: string = this.httpBaseUrl + "ErrorLog/";


  /*Survey URLs*/
  getValidationData: string = this.httpBaseUrl + this.getValidations + "GetValidationDetails";
  getUserDashboard: string = this.httpBaseUrl + this.surveyController + "GetDashboard";
  getTemplateData: string = this.httpBaseUrl + this.surveyController + "GetTemplateData";
  getSurveyUsers: string = `${this.httpBaseUrl}${this.surveyController}GetSurveyUsers`;
  createSurvey: string = this.httpBaseUrl + this.surveyController + "CreateSurvey";
  updateSurvey: string = this.httpBaseUrl + this.surveyController + "UpdateSurvey";
  archiveSurvey: string = this.httpBaseUrl + this.surveyController + "ArchiveSurvey";
  restoreSurvey: string = this.httpBaseUrl + this.surveyController + "RestoreSurvey";
  removeTemplate: string = this.httpBaseUrl + this.surveyController + "RemoveTemplate";
  uploadSurveyImage: string = this.httpBaseUrl + this.surveyController + "UploadSurveyImage";
  setFavoriteSurvey: string = this.httpBaseUrl + this.surveyController + "SetFavouriteSurvey";
  accountRegistration: string = this.httpBaseUrl + this.accountController;
  postlogin: string = this.httpBaseUrl + this.loginController + "login";
  postregister: string = this.httpBaseUrl + this.accountController;
  getSurveyById: string = this.httpBaseUrl + this.surveyViewController;
  postquestions: string = this.httpBaseUrl + this.surveyController + "PostQuestions";
  saveRespondents: string = this.getSurveyById + '/SaveRespondents';
  savecolorscheme: string = this.httpBaseUrl + this.surveyController + "SaveSurveyTemplateColor";
  craetetemplate: string = this.httpBaseUrl + this.surveyController + "SaveSurveyAsTemplate";
  copySurvey: string = this.httpBaseUrl + this.surveyController + "CopySurvey";
  getpopularData: string = this.httpBaseUrl + this.surveyController + "GetTopFavouriteSurvey";
  /* Validation Settings URLs*/

  getAllValidations: string = this.validationController + "GetValidationDetails";
  createValidation: string = this.validationController + "CreateValidation";
  updateValidation: string = this.validationController + "UpdateValidation";
  deleteValidation: string = this.validationdelController + "/DeleteSurveyDetails";

  /* User Management URLs*/
  getAllUsers: string = this.userManagementController + "GetCustomers";
  getAllUsersGroupRole: string = this.userManagementController + "GetUserGroupRole";
  getAllGroups: string = this.userManagementController;
  getAllOrgs: string = this.userManagementController + "GetOrganizations";
  getAllGrpByOrg: string = this.userManagementController + "GetGroupByUserId";
  createGroup: string = this.userManagementController + "CreateGroup";
  createOrganization: string = this.userManagementController + "CreateOrganization";
  updateGroup: string = this.userManagementController;
  deleteGroup: string = this.userManagementController + "DeleteGroup";
  getUser: string = this.userManagementController + "getUser";
  getAllUserRoles: string = this.userManagementController + "GetUserRoles";
  updateUserRole: string = this.userManagementController + "UpdateUserRole";
  updateDefaultUserRole: string = this.userManagementController + "UpdateDefaultUserRole";
  getGroupsByOrgId: string = this.userManagementController + "GetGroupByOrgnizationId";
  updateGroupName: string = this.userManagementController + "UpdateGroupName";
  /* Survey Response URLs*/
  initiateSurveyResponse: string = this.surveyResponseController + "InitiateSurvey";
  SubmitSurveyResponse: string = this.surveyResponseController + "SubmitSurvey";

  /* Survey Result URLs*/
  getSurveyResponses: string = this.surveyResultController + "GetSurveyResponses";

  /*Log Exception*/
  logException: string = this.errorLogController + "LogException";

}
