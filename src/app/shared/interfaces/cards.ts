export interface Card {
  surveyId: string;
  name: string;
   description: string;
   isFavourite: boolean;
   resultsCount: string;
   status: string;
   statusDesc: string;
   accessLevel: string;
   createdDate: Date;
   surveyGroupName: string;
   surveyGroupId: string;
   isArchived: boolean;
   isLiked: boolean;
   categoryName: string;
   categoryId: number;
   surveyType: string;
}


export interface ValidationDetail {
  validationId: number;
  title: string;
  description: string;
  regularExpression: string;
  errorMessage: string;
  validationType: string;
  format: string;
  minRange: number;
  maxRange: number;
  isCharacterAllowed: true;
  isNumericAllowed: true;
  isSpecialCharacterAllowed: true;
  excludeCharacters: string;
}

export interface ErrorMessages {
  [key: string]: string;
}
