/* Author : Hariom Sinha 557173 */

import { Input } from "@angular/core";
import { Question } from "./models/sectionview.model";

/* This module will be an utility for the Condiitonal Controls  */
export class ConditionalControls{
  /*
    FLOW : 
    Radio Control,  Drop down, Multi Select.

    1. For any control, on *init if parentQuestionGuid exists, then question Object will be passed
       to this utility class. Or else always visible.
    
    2. The utility class will have some methods that will emit true or false based on the 
       output from the below Algo : 

       method input : question
       validations to be performed within : parentQuestionGuid and its response. If the response
                                            matches the input in that control, then in that case
                                            the emit value will be true or else, false.
       bounding values : Iteration Over Sequence ( i < sequenceNumberOfThatCOntrol, i belongs to
                          every sequence number in all Parent Controls).
       output : True or False..
    3. If true -- >  show or else Hide..                            
  */
  static allQuestions = [];
  static latestquestion;
  constructor(){
  }

  public updateSectionsInConditionalQuestions(allQuestions){
    ConditionalControls.allQuestions = allQuestions;
  }

  public checkConditionalControl(questionToValidate){
    var questions = [];
    questions =  ConditionalControls.allQuestions['questions'];
    for(var question in questions){
        if( questions[question]['sequenceNumber']<questionToValidate.sequenceNumber
            && questions[question]['questionGuid'] === questionToValidate.parentQuestionGuid)
            { 
              // console.log("1");
              // console.log(ConditionalControls.latestquestion);
              // console.log(questionToValidate.parentQuesResponse);
              // if(latestUpdatedQuestion['defaultResponse'] === questionToValidate.parentQuesResponse)
              //   return true;
            }
    }
    return false;
  }

  public setLatestUpdateQuestion(question){
    // console.log("change came");
    ConditionalControls.latestquestion = question;
    // console.log(ConditionalControls.latestquestion);
    
  }
  
  
}