export class ValidationViewModel {
    public validationId: number;
    public title: string;
    public description: string;
    public regularExpression: string;
    public errorMessage: string;
    public validationType: string;
    public format: string;
    public minRange: number;
    public maxRange: number;
    public isCharacterAllowed: boolean = false;
    public isNumericAllowed: boolean = false;
    public isSpecialCharacterAllowed: boolean = false;
    public excludeCharacters: string;
}   