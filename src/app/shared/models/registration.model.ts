export class RegistrationModel {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    location : string;
    organizationId: string;
}
