export class CardModel {
  public surveyId: string;
  public name: string;
  public description: string;
  public isFavourite: boolean;
  public resultsCount: string;
  public status: string;
  public statusDesc: string;
  public accessLevel: string;
  public createdDate: Date;
  public surveyGroupName: string;
  public surveyGroupId: string;
  public isArchived: boolean;
  public isLiked: boolean;
  public categoryName: string;
  public categoryId: number;
  public surveyType: string;
}
