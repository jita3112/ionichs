import { ValidationViewModel } from './validationview.model';

export class QuestionViewModel {
  public questionId: number;
  public sequenceNumber: number;
  public dataType: string;
  public inputType: string;
  public refTableName: string;
  public questionText: string;
  public questionTextShort: string;
  public isQuestionRequired: string;
  public questionToolTip: string;
  public parentQuestionId: number;
  public parentQuesResponse: string;
  public defaultResponse: string;
  public maxLength: number;
  public sectionId: number;
  public options: Option[];
  public questionImage: (string | ArrayBuffer);
  public questionImageName: string;
  public validations: number[];
  public validationDetails: ValidationViewModel[];
  public isParentQuestionVisible:boolean;
  public isSecParentQuestionVisible:boolean;
  public futureDateEnabled?: boolean;
  public pastDateEnabled?: boolean;
  public todayDateEnabled?: boolean;
  public questionGuid : string;
  public responseText: string;
}

export class Option {
  public key?: string;
  public value: string;
  //public selected?: boolean;
}
