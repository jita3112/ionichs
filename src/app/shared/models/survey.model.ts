import {QuestionViewModel} from './questionview.model'
 import {SectionViewModel} from './sectionview.model';
export class SurveyDetail {
    surveyId: string;
    name: string;
    status: string;
    description: string;
    resultsCount: number;
    isFavourite: boolean;
    isLiked: boolean;
    categoryName: string;
    colorScheme: string;
    surveyType: string;
    logoFormat: string;
    logoImage: string;
    surveyAdmins: SurveyAdmins[];
    sections: SectionViewModel[];
    createdDate: Date;
    surveyGroupName: string;
    surveyGroupId: string;
  }
export class SurveyAdmins {
    fullName: string;
    userId: string;
  } 
 

export class QuestionAnswer {
    sectionId = '';
    parentSectionGuid = '';
    parentSectionId: number;
    parentQuestionGuid = '';
    parentQuesResponse = '';
    gridQuestions: GridQuestion[] = [];
    sectionName: string = null;
    isExpanded: boolean = null;
    questions: Question[];
    isMenuVisible = true;
    sequenceNumber = 1;
    sectionDescription?: string = null;
    sectionToolTip?: string = null;
    isDeleted: boolean = null;
    constructor(title, ques, sectionId) {
      this.sectionId = sectionId;
      this.sectionName = title;
      this.isExpanded = false;
      this.questions = ques;
      this.isMenuVisible = false;
    }
  }

export class GridQuestion{
    questions: Question[];
    gridId?: number;
    description?: string;
    sequenceNumber?: number;
    isExpanded = false;
    isMenuVisible = false;
    isDeleted = false;

  }

export class Question {
    questionId?: number;
    sequenceNumber = 1;
    questionTextShort?: string;
    isQuestionRequired?: string = null;
    questionText: string;
    dataType: string;
    inputType: string = null;
    refTableName = 'Survey Questions';
    questionToolTip = '';
    parentQuestionId?: number;
    parentQuesResponse = '';
    defaultResponse = '';
    maxLength?: number = null;
    isMaxLength = false;
    sectionId?: number = null;
    gridId?: number = null;
    isExpanded = true;
    questionImage?: (string | ArrayBuffer);
    questionImageName = '';
    options?: OptionsInfo[];
    editQuestion = true;
    validations: number[] = [];
    isValidation: boolean;
    isDeleted: boolean = null;
    futureDateEnabled = true;
    pastDateEnabled = true;
    todayDateEnabled = true;
    questionGuid = '';
    parentQuestionGuid = '';
    userResponse = '';
    validationDetails: ValidationDetail[];
    constructor(title, tag, type, questionGuid) {
      this.questionGuid = questionGuid;
      this.questionText = title;
      this.questionTextShort = tag;
      this.dataType = type;
      this.isExpanded = false;
      this.options = [{
        value: ''
      }];
      this.validations = [];
      this.questionImage = '';
      this.editQuestion = false;
      this.isValidation = false;
    }
  }

export class OptionsInfo {
    key?: string;
    value: string;
  }

export class QuestionType {
    icon: string;
    title: string;
    description: string;
    colorCode: string;
    type: string;
  }

export class ValidationData {
    title: string;
    validationId: number;
  }

export class SelectedQuestionData {
    isSectionSelected: boolean;
    selectedCard: Question;
    selectedSection: QuestionAnswer;
    questions: QuestionAnswer[];
  }

export class ValidationDetail {
  validationId: number = null;
  title: string = null;
  description: string = null;
  regularExpression: string = null ;
  errorMessage: string = null;
  validationType: number = null;
  format: string = null ;
  minRange: number = null;
  maxRange: number = null;
  isCharacterAllowed: boolean;
  isNumericAllowed: boolean;
  isSpecialCharacterAllowed: boolean;
  excludeCharacters: string = null;
}

export class SurveyResponse
{
  surveyId: string;
  surveyResponseID: string;
  questionResponses: SurveyQuestionResponse[];
  groupId:string;
  allowanonymousResponse:boolean=false;
}

export class SurveyQuestionResponse
{
  questionId: number;
  questionReponse: string;
}
