export enum ButtonType {
    Rectangle = 0,
    Round=1
}
export enum ButtonActionType {
    Submit = 0,
    Button = 1
  }
export enum ButtonColors {    
    Default = 0,
    Yellow = 1,
    Blue = 2,
    Red = 3,
    White=4
}
export enum ButtonStyle {
    PlusIcon = 0,
    MinusIcon = 1,
    Textonly = 2,
    PlusIconWithText = 3,
    MinusIconWithText=4
}
export enum ButtonSize {
  Small = 0,
  Medium = 1,
  Large = 2,
  Xlarge = 3
}
