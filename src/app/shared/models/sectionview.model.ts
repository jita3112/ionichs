import { QuestionViewModel } from './questionview.model';

export class SectionViewModel {
    public sectionId: number;
    public sectionName: string;
    public sequenceNumber: number;
    public sectionDescription: string;
    public sectionToolTip: string;
    public gridQuestions:GridViewModel[];
    public questions: QuestionViewModel[];
    public parentQuesResponse: string;
    
}

export class GridQuestion{
    questions:Question[];
    gridId?:number;
    description?:string;
    sequenceNumber?:number;
    isExpanded: boolean = false;
    isMenuVisible: boolean = false;
    isDeleted:boolean=false;
  
  }
  
export class Question {
    questionId?: number;  
    sequenceNumber?: number = 1;
    questionTextShort?: string;
    isQuestionRequired?: string = null;
    questionText: string;
    dataType: string;
    inputType: string = null;
    refTableName?: string = "Survey Questions";
    questionToolTip?: string = "";
    parentQuestionId?: number;
    parentQuesResponse?: string = "";
    defaultResponse?: string = "";
    maxLength?: number = null;
    isMaxLength: boolean = false;
    sectionId?: number = null;
    gridId?:number=null;
    isExpanded: boolean = true;
    questionImage?: (string | ArrayBuffer);
    questionImageName?: string = "";
    options?: OptionsInfo[];
    editQuestion: boolean = true;
    validations: number[] = [];
    isValidation: boolean;
    isDeleted: boolean = null;
    futureDateEnabled?: boolean = true;
    pastDateEnabled?:boolean = true;
    todayDateEnabled?:boolean = true;
    questionGuid:string="";
    parentQuestionGuid:string="";
    constructor(title, tag, type,questionGuid) {
      this.questionGuid=questionGuid;
      this.questionText = title;
      this.questionTextShort = tag;
      this.dataType = type;
      this.isExpanded = false;
      this.options = [{
        value: ''
        //selected:false
      }];
      this.validations = [];
      this.questionImage = '';
      this.editQuestion = false;
      this.isValidation = false;
    }
  }
  
export class OptionsInfo {
    key?: string;
    value: string;
    //selected?: boolean;
  }
  
  export class QuestionType {
    icon: string;
    title: string;
    description: string;
    colorCode: string;
    type: string;
  }
  
export class GridViewModel{

   public gridId: number;
   public sequenceNumber: number;
   public response:ResponseModel[];
   public questions: QuestionViewModel[];
}

export class ResponseModel
{
    public isDefaultControl:boolean=true;
    public sequenceNumber: number;
    public questions: QuestionViewModel[];
}