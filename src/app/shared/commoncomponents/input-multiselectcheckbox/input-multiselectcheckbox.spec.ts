import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
 

import { InputMultiSelectCheckboxComponent } from './input-multiselectcheckbox.component';

describe('InputCheckboxComponent', () => {
  let component: InputMultiSelectCheckboxComponent;
  let fixture: ComponentFixture<InputMultiSelectCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputMultiSelectCheckboxComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InputMultiSelectCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
