import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgModel, FormBuilder, FormGroup,FormArray ,FormControl} from '@angular/forms';
import { IonInput, IonSelect, IonSelectOption } from '@ionic/angular';
import { ErrorMessages } from './../../interfaces/cards';
 
@Component({
  selector: 'app-input-multiselectcheckbox',
  templateUrl: './input-multiselectcheckbox.component.html',
  styleUrls: ['./input-multiselectcheckbox.component.scss'],
})
export class InputMultiSelectCheckboxComponent implements OnInit {
  
  conditionalShowHide : boolean = false;
  displayError:boolean=false;
  value: string=null;
  isValueEntered:boolean=false;
  customValidationMessages: ErrorMessages = {};
  multiselectform: FormGroup;
  selectionData = [];

  @Input() label: string;
  @Input() options: any;
   @Input() required: boolean;
  @Output() emitValue = new EventEmitter();
  @Input() control: NgModel;
  @ViewChild('multiSelectModel') multiSelectModel: NgModel;
  @Input() question:any;
  @Input() isGridComponent: boolean;

  
  constructor(private formBuilder: FormBuilder) {
    this.multiselectform = this.formBuilder.group({
      selection: new FormArray([])
    });
  }
     
  ngOnInit() {
    
    if (this.required)
    { 
      this.question["Valid"]=false;
    }
    
  }
  returnValue(control){
     
    let selection='';
     control.value.forEach(value => {
      if (selection==='')
      {
        selection=value;
      }
      else{
        selection = selection+"||" +value;
      
      }
      });
    this.displayError=selection==='' && this.required;
    this.question["Valid"]=!this.displayError;
    this.question.defaultResponse = selection;
    this.emitValue.emit(this.question);
  }
  onCheckboxChange(e:Event) {
    // debugger;
    var target= (<HTMLInputElement>e.target);
    this.returnValue(target);
  }
  
}
