import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgModel } from '@angular/forms';
import { EventPassService } from 'src/app/event-pass.service';
import { Validation } from '../../Validation';
import { ErrorMessages } from './../../interfaces/cards';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss'],
})
export class InputTextComponent implements OnInit {
  @Input() isGridComponent: boolean;
  customValidationMessages: ErrorMessages = {};
  value: string;
  maxlength: number;
  custom_error_msg : any = "Wrong Input";
  result : any = {
    "error": [],
    "validationStatus": true
  };

  @Input() label: string;
  @Input() required: boolean;
  @Input() minlength: number;
  @Input() errorMessage: ErrorMessages;
  @Input() className: string;
  @Input() placeholder: string;
  @Input() control: NgModel;
  @Input() pattern: RegExp;
  @Input() triggerErrorDesc = true;
  @Input() IsNumberic: boolean;
  @Input() question : any;


  @ViewChild('inputElementModel') inputElementModel: NgModel;

  constructor(public appEventPass : EventPassService) {
   }

  ngOnInit() {
    this.result["error"] = [];
    this.result["validationStatus"] = true;
    if (this.maxlength === 0) {
      this.maxlength = 100;
    }
  }

  private setValidationMessages(): void {
    const customErrorMsg = this.errorMessage;

    this.customValidationMessages.mandatory = customErrorMsg && customErrorMsg.mandatory;
    this.customValidationMessages.minlength = customErrorMsg && customErrorMsg.minlength;
    this.customValidationMessages.maxlength = customErrorMsg && customErrorMsg.maxlength;
    this.customValidationMessages.pattern = customErrorMsg && customErrorMsg.pattern;
  }

  /**
   * method to handle the key up event
   * @method textfieldKeyUpHandler
   * @param event
   * @return none
   */
  public textfieldKeyUpHandler(event: Event): void {
    this.value = ((<HTMLInputElement>event.target).value);
    if(this.value.length === 0){
      this.result["validationStatus"] = true;
    }

    //for custom Validation
    if(this.question.validationDetails.length !=0){
        var validater : Validation = new Validation();
        this.result = validater.checkCustomValidationsIfAny(this.value, this.question); //get True Or False Based on Custom Validation
        if(this.value.length === 0){
          this.result["validationStatus"] = true;
        }
        this.appEventPass.send("questionValidity", this.result["validationStatus"]);
    }
    this.question["Valid"]=!(this.value==='');
  }
}
