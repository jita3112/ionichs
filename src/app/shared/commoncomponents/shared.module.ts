import { NgModule } from '@angular/core';
import { InputTextComponent } from './input-text/input-text.component';
import { InputRadioComponent } from './input-radio/input-radio.component';
import { InputDropdownComponent } from './input-dropdown/input-dropdown.component';
import { InputDateComponent } from './input-date/input-date.component';
import{InputMultiSelectCheckboxComponent} from './input-multiselectcheckbox/input-multiselectcheckbox.component';
import{InputCheckboxComponent} from './input-checkbox/input-checkbox.component';
import {InputRatingComponent} from './input-rating/input-rating.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { GetArrayOfNumbers } from './../numbers.pipe';
import {GridComponent} from './input-grid/input-grid.component';
const sharedComponents = [
        InputTextComponent,
        InputRadioComponent,
        InputDropdownComponent,
        InputCheckboxComponent,
        InputDateComponent,
        InputMultiSelectCheckboxComponent,
        InputRatingComponent,
        GetArrayOfNumbers,
        GridComponent
];

@NgModule({
    declarations: [
        ...sharedComponents
    ],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
    ],
    exports: [
        ...sharedComponents
    ]
  })
  export class SharedModule {}
