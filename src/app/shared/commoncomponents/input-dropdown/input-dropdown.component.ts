import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-input-dropdown',
  templateUrl: './input-dropdown.component.html',
  styleUrls: ['./input-dropdown.component.scss'],
})
export class InputDropdownComponent implements OnInit {
  @Input() label: string;
  @Input() options: any;
  @Output() emitValue = new EventEmitter();
  @ViewChild('comboElementModel') comboElementModel: NgModel;
  @Input() question : any;
  @Input() isGridComponent: boolean;
  value: string;


  constructor() { }

  ngOnInit() {

  }

  setValue(event: Event){
    this.value = ((<HTMLInputElement>event.target).value);
    this.question["Valid"]=!(this.value===undefined);
    this.question.defaultResponse = this.value;
    this.emitValue.emit(this.question);
  }

}
