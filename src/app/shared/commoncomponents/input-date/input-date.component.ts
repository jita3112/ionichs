import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss'],
})
export class InputDateComponent implements OnInit {
  @Input() isGridComponent: boolean;
  @Input() value: string;
  @Output() emitValue = new EventEmitter();
  @Input() question : any;
  @Input() label: string;
  errormgvisible : boolean ;
  customErrorMsg : string = "";
  constructor() { }
  dateControlClass:string="";
  ngOnInit() {
    this.errormgvisible = false;
    this.dateControlClass=this.isGridComponent?"input-wrapper":"input-wrapper marginTop";
    
  }

  setDateValue(event: Event){
    this.value = ((<HTMLInputElement>event.target).value);
    let currentDate=new Date();
    let newDate = new Date(this.value);
    newDate.setHours(0,0,0,0);
    currentDate.setHours(0,0,0,0);
    this.question["Valid"] = true;
    this.errormgvisible = false;
    this.customErrorMsg = "";

    if (!this.question.pastDateEnabled && newDate<currentDate)
    {
      this.errormgvisible = true;
      this.customErrorMsg = "Past Date is not allowed";
      this.question["Valid"] = false;
    }
    if (!this.question.todayDateEnabled  && newDate.toString()==currentDate.toString())
    {
      this.errormgvisible = true;
      this.customErrorMsg = "Today's Date is not allowed";
      this.question["Valid"] = false;
    }
    if (!this.question.futureDateEnabled  && newDate>currentDate)
    {
      this.errormgvisible = true;
      this.customErrorMsg = "Future Date is not allowed";
      this.question["Valid"] = false;
    }
    //converting date format for response
    var responseDate = newDate.toISOString().slice(0,10);
    this.question.defaultResponse = responseDate;
    if(this.question["Valid"])
      this.emitValue.emit(this.question);
  }
}
