import { Component, OnInit,Input, Output, EventEmitter  } from '@angular/core';
import { GridViewModel,ResponseModel } from '../../models/sectionview.model';
 
import { QuestionViewModel,Option } from '../../models/questionview.model';
 
import { ButtonColors, ButtonSize, ButtonStyle, ButtonType } from '../../models/button.model';

@Component({
  selector: 'app-grid',
  templateUrl: './input-grid.component.html',
  styleUrls: ['./input-grid.component.scss']
})
export class GridComponent implements OnInit {
  @Input() gridQuestions: GridViewModel;
  @Input() header:ResponseModel;
  @Input() questionIndex: number;
  @Input() isGridComponent: boolean;
  @Input() totalQuestion: number;
  @Input() foregroundcolor: string;
  @Input() backgroundcolor: string;
  @Input() theme: string;
  @Input() controlBGColor: string;
  @Input() controlBorder: string;
  @Input() TextColor: string;
  @Input() tableBorder: string;
  @Input() label: string;
  btnRnd = ButtonType.Round;
  btnPlus = ButtonStyle.PlusIcon;
  btnLarge = ButtonSize.Large;
  btnColor = ButtonColors.Blue;
  maxSequenceNumber:number=1;
  conditionQues: ConditionalQues[] = [];
  Header:string[];
  questionLength:Number;
  constructor() { 
  }

  RemoveResponse(gridques:any)
  {
    
    const index: number = this.gridQuestions.response.indexOf(gridques);
    if (index !== -1) {
        this.gridQuestions.response.splice(index, 1);
    }        
  }

  addNewResponse(gridques:ResponseModel)
  {    
    debugger;

    var addResponse=new ResponseModel();
    addResponse.sequenceNumber=this.gridQuestions.response.length+1;    
    addResponse.questions=[];
    var index=[];
    gridques.questions.forEach(element => {
      index.push(element.questionId);
    });
     var maxindex=0;
  
    gridques.questions.forEach(element => {
      var que=new QuestionViewModel();
      que.questionId=index[maxindex];
      que.questionImage=element.questionImage;
      que.parentQuestionId=element.parentQuestionId;
      que.questionImageName=element.questionImageName;
      que.questionText=element.questionText;
      que.questionTextShort=element.questionTextShort;
      que.questionToolTip=element.questionToolTip;
      que.refTableName=element.refTableName;
      que.sectionId=element.sectionId;
      que.sequenceNumber=element.sequenceNumber;
      que.validationDetails=element.validationDetails;
      que.validations=element.validations;
      que.dataType=element.dataType;
      que.defaultResponse=element.defaultResponse;
      // que.inputType="rot";
      que.inputType=element.inputType;
      que.isQuestionRequired=element.isQuestionRequired;
      que.options =[];
      element.options.forEach(elem=>{
        var a=new Option;
        a.key=elem.key;
        a.value=elem.value;
        que.options.push(a);
      });
     
    
      que.parentQuesResponse = element["parentQuesResponse"];
     // que.responseText = element["responseText"];
      que["initialText"] = element["initialText"];
      que["finalText"] = element["finalText"];
      que["responseText"]=null;
      que.maxLength=element.maxLength;
      maxindex=maxindex+1;

      addResponse.questions.push(que);
      addResponse.isDefaultControl=false;
    });
     
    this.gridQuestions.response.push(addResponse);
    this.gridQuestions.response.sort((a, b) => (a.sequenceNumber > b.sequenceNumber) ? 1 : -1);
    this.maxSequenceNumber=this.gridQuestions.response.length;
  }
  updateRatingConfig(config, que) {
    
  }
  ngOnInit() {
    this.questionLength=this.header.questions.length;
    console.log(this.gridQuestions);
  }
  // GetMulChildData(selectedChild:any,reponse:any)
  // {
  //   debugger;
  //   this.GetChildData(selectedChild);
  // }
  GetChildData(selectedChild: any) {
    debugger;
    selectedChild.options.forEach((e) =>
    {
      //Update value as selected in conditional ques arr
      this.conditionQues.forEach((element) => {
        if (element.value == e.value && e.selected == true) {
            element.isSelected = true;

          this.gridQuestions.response.forEach((parobj, parindex) => {
            parobj.questions.forEach((childobj, childindex) => {
              if (childobj["questionTextShort"] === element.que && element.isSelected == true) {
                childobj["isParentQuestionVisible"] = true;
              }
            });
          });
        }
        else if (element.value == e.value && e.selected == false){
          element.isSelected = false;

          this.gridQuestions.response.forEach((parobj, parindex) => {
            parobj.questions.forEach((childobj, childindex) => {
              if (childobj["questionTextShort"] === element.que  && element.isSelected == false) {
                childobj["isParentQuestionVisible"] = false;
              }
            });
          });
        }
      });
    });
    //console.log(this.gridQuestions);
  }

  private SetChoiceData(question:any,value:any)
  {
    question=value;  
  }
}
export class ConditionalQues {
  que: string;
  value: string;
  isSelected?: boolean = false;
}