import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgModel, FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { IonInput, IonSelect, IonSelectOption } from '@ionic/angular';
import { ErrorMessages } from '../../interfaces/cards';

@Component({
  selector: 'app-input-checkbox',
  templateUrl: './input-checkbox.component.html',
  styleUrls: ['./input-checkbox.component.scss'],
})
export class InputCheckboxComponent implements OnInit {

  displayError: boolean = false;
  value: string = null;
  isValueEntered: boolean = false;
  customValidationMessages: ErrorMessages = {};
  @Input() label: string;
  @Input() options: any;
  @Input() required: boolean;
  @Input() isGridComponent: boolean;
  @Output() emitValue = new EventEmitter();
  @Input() control: NgModel;
  //@Input() triggerErrorDesc :boolean;
  @ViewChild('multiSelectModel') multiSelectModel: NgModel;
  //@Input() form:NgForm ;
  multiselectform: FormGroup;
  selectionData = [];
  selection = "";
  @Input() question: any;

  constructor(private formBuilder: FormBuilder) {
    this.multiselectform = this.formBuilder.group({
      selection: new FormArray([])
    })
  }
  ngOnInit() {
    if (this.required) {
      this.question["Valid"] = false;
    }
  }
  returnValue(controllerData) {
    if (this.selection === '') {
      this.selection = controllerData.value;
    }else {
      this.selection = this.selection + "||" + controllerData.value;
    }
    this.displayError = this.selection === "";
    this.question["Valid"] = !this.displayError;
    this.question.defaultResponse = this.selection;
    this.emitValue.emit(this.question);
  }
  onCheckboxChange(e: Event) {
    var target = (<HTMLInputElement>e.target);
    this.returnValue(target);
  }
}
