import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgModel } from '@angular/forms';
import { EventPassService } from 'src/app/event-pass.service';


@Component({
  selector: 'app-input-radio',
  templateUrl: './input-radio.component.html',
  styleUrls: ['./input-radio.component.scss'],
})
export class InputRadioComponent implements OnInit {
  @Input() isGridComponent: boolean;
  @Input() label: string;
  @Input() options: any;
  @Input() value: string;
  @Output() emitValue = new EventEmitter();
  @Input() question : any;
  @Input() required: boolean;
  conditionalShowHide: boolean = false;
  
  @ViewChild('radioElementModel') radioElementModel: NgModel;
  constructor(public appEventPass : EventPassService) { }

  ngOnInit() {
  }

  setValue(event: Event){
    console.log("inside radio ip");
    this.value = ((<HTMLInputElement>event.target).value);
    this.question["Valid"]=!(this.value==='' && this.required);
    this.question.defaultResponse = this.value;
    this.emitValue.emit(this.question);
  }
}
