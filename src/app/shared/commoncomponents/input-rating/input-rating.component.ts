import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgModel, FormBuilder, FormGroup,FormArray ,FormControl} from '@angular/forms';
import { ErrorMessages } from '../../interfaces/cards';
 
@Component({
  selector: 'app-rating-view',
  templateUrl: './input-rating.component.html',
  styleUrls: ['./input-rating.component.scss'],
})
export class InputRatingComponent implements OnInit {
  @Input() isGridComponent: boolean;
  @Output() updateConfig = new EventEmitter<RatingResponse>();
  @Input() isEditing = false;
  @Input() showName?: boolean=true;

  getNumbers: number = 10;

  @Input() question: any;
  @Input() questionIndex: number;
  @Input() totalQuestion: number;
  minLength: number = 0;
  maxLength: number = 999;
  errormgvisible: boolean = false;
  validationerrormessage: string[] = [];
  isConditionIsTrue: boolean;
  //validationPattern: string = "[0-9]{1,999}"
  //validationPattern: string = "^[a-zA-Z0-9!@#$&%()\\-`.+,/\"]*$"
  validationPattern: string = "[A-Z0-9!@#$&%()\\-`.+,/\"^a-z]{1,999}"
  checkallvalidations: string = "";
  numberPattern: string = "0-9";
  alphabetsPattern: string = "A-Za-z";
  specialCharactersPattern: string = "!@#$&()\\-`.+,/\"*$%_^";
  isNumberValidation: boolean = false;
  isCharactersValidation: boolean = false;
  isSpecialCharactersValidation: boolean = false;
  excludeCharacters: string;
  isExcludeCharactersValidation: boolean = false;
  controlColor: string;
  @Input() foregroundcolor: string;
  @Input() backgroundcolor: string;
  @Input() theme: string;
  @Input() controlBGColor: string;
  @Input() controlBorder: string;
  @Input() TextColor: string;
  @Input() tableBorder: string;
  
  constructor(private formBuilder: FormBuilder) {
     
  }
  ngOnInit() {
 
    if (this.question.isQuestionRequired == "Y") {
      this.isConditionIsTrue = true;
    }
    else {
      this.isConditionIsTrue = false;
    }

    this.question.validationDetails.forEach((obj, index) => {
      //to add number pattern only once
      if (this.isNumberValidation === false) {
        if (obj.isNumericAllowed === true) {
          this.isNumberValidation = true;
          this.checkallvalidations += this.numberPattern;
        }
      }
      if (this.isCharactersValidation === false) {
        if (obj.isCharacterAllowed === true) {
          this.isCharactersValidation = true;
          this.checkallvalidations += this.alphabetsPattern;
        }
      }
      if (this.isSpecialCharactersValidation === false) {
        if (obj.isSpecialCharacterAllowed === true) {
          this.isSpecialCharactersValidation = true;
          this.checkallvalidations += this.specialCharactersPattern;
        }
      }
      if (this.isExcludeCharactersValidation === false) {
        if (obj.excludeCharacters != null && obj.excludeCharacters != "") {
          this.isExcludeCharactersValidation = true;
          this.excludeCharacters = obj.excludeCharacters;
        }
      }
      this.validationerrormessage.push(obj.errorMessage);
      //check min length and max length
      if (obj.minRange != null && obj.minRange >= 0 && obj.maxRange != null && obj.maxRange > 0) {
        this.minLength = obj.minRange;
        this.maxLength = obj.maxRange;

      }
      if (obj.regularExpression != null && obj.regularExpression != "") {
        this.validationPattern = obj.regularExpression + "{1,999}";
      }
    });
    if (this.checkallvalidations != "" && this.checkallvalidations != null) {
      this.validationPattern = "[" + this.checkallvalidations + "]{1,999}"
    }

    // if(this.theme=="Theme1")
    // {
    //   this.controlColor ="rgb(187 209 193);";
    //   this.controlBorder ="2px solid #162f29";
    // }
    // else if(this.theme=="Theme2"){
    //   this.controlColor ="rgb(199 192 235);";
    //   this.controlBorder ="2px solid #282cb1";
    // }
    // else{
    //   this.controlColor ="#dbd0ab";
    //   this.controlBorder ="2px solid #2e90a3";
    // }
    
  }

  emitUpdatedData() {
    this.updateConfig.emit(this.question);
  }
    
updateValue(val) {debugger
  this.question.rating = val;
  this.updateConfig.emit(this.question);
  this.question["responseText"] = val;
  this.question.defaultResponse=val;
  if (this.question.isQuestionRequired == "Y") {
    if (this.question["responseText"] == null || this.question["responseText"] == "") {
      this.question["Valid"] = false;
    }
    else {
      this.question["Valid"] = true;
    }
  }
  else {
    this.question["Valid"] = true;
  }
}
}

export class RatingResponse {
  initialText?: string;
  finalText?: string;
  rating?: number;
}
