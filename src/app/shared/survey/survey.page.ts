import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SurveyDetail, SurveyResponse, SurveyQuestionResponse } from '../models/survey.model';
import { ManageSurveysService } from '../services/manage-surveys.service';
import { Toastservice } from '../../shared/services/toast.service';
import { Form, NgForm } from '@angular/forms';
import { GridViewModel, ResponseModel, SectionViewModel } from '../models/sectionview.model';
import { QuestionViewModel, Option } from '../models/questionview.model';
import { InputRadioComponent } from '../commoncomponents/input-radio/input-radio.component';
import { EventPassService } from 'src/app/event-pass.service';
 

@Component({
  selector: 'app-survey',
  templateUrl: './survey.page.html',
  styleUrls: ['./survey.page.scss'],
})
export class SurveyPage implements OnInit {

  @Input() surveyDetail: SurveyDetail;
  sections: any;
  currentSection: any;
  currentSectionIndex: number;
  surveyId: string;
  surveyName: string;
  surveyDescription: string;
  surveyResponse: SurveyResponse;
  surveyStarted = false;
  lastSection = false;
  formError: any;
  AllFieldsValid = false;
  displayFormError = false;
  blockSlide : boolean = true;
  @ViewChild('surveyForm') surveyForm: NgForm;
  index : any = 0;

  constructor(private modalCtrl: ModalController,
    private manageSurvey: ManageSurveysService,
    private toastservice: Toastservice,
    public appEventPass : EventPassService
    ) { 
      this.appEventPass.subscribe(this.questionValidityStatus.bind(this));
    }

  ngOnInit() {
    this.surveyResponse = new SurveyResponse();
    this.surveyResponse.questionResponses = [];
    this.surveyResponse.surveyId = this.surveyDetail.surveyId;
    this.blockSlide = true;
  }
  private structureSections(surveyDetail) {
    this.surveyId = surveyDetail.surveyId;
    this.surveyName = surveyDetail.name;
    this.surveyDescription = surveyDetail.surveyDescription;
    this.surveyDetail.sections.sort(p => p.sequenceNumber);
    var surveysectionlist = this.surveyDetail.sections;
    this.surveyDetail.sections = [];
    surveysectionlist.forEach(section => {

      section.questions.sort(q => q.sequenceNumber);
      let sections = new SectionViewModel();
      sections = Object.assign(sections, section);
      sections.gridQuestions = [];
      sections = section;
      //pass latest Questions to the COnditional Questions Utility Class
      section.questions.forEach(x => {
        if (x.isQuestionRequired === "Y") {
          x["Valid"] = false;
        }
        console.log(x);
      })

      //grid 
      section.gridQuestions.forEach(function (x) {
        var gd = new GridViewModel();
        gd.gridId = x.gridId;
        gd.sequenceNumber = x.sequenceNumber;
        gd.response = [];
        var response = new ResponseModel();
        response.questions = [];
        x.questions.forEach(function (childobj) {
          let questions = new QuestionViewModel
          questions.questionId = childobj["questionId"];
          questions.sequenceNumber = childobj["sequenceNumber"];
          questions.dataType = childobj["dataType"];
          questions.inputType = childobj["inputType"];
          questions.refTableName = childobj["refTableName"];
          questions.questionText = childobj["questionText"];
          questions.questionTextShort = childobj["questionTextShort"];
          questions.isQuestionRequired = childobj["isQuestionRequired"];
          questions.questionToolTip = childobj["questionToolTip"];
          questions.parentQuestionId = childobj["parentQuestionId"];
          questions.parentQuesResponse = childobj["parentQuesResponse"];
          questions.defaultResponse = childobj["defaultResponse"];
          questions.maxLength = childobj["maxLength"];
          questions.sectionId = childobj["sectionId"];
          questions.options = childobj["options"];
          questions.questionImage = childobj["questionImage"];
          questions.questionImageName = childobj["questionImageName"];
          questions.responseText = childobj["responseText"];
          questions["initialText"] = childobj["initialText"];
          questions["finalText"] = childobj["finalText"];
          questions.validations = childobj["validations"];
          // questions.validationDetails = childobj["validationDetails"];
          questions.futureDateEnabled = childobj["futureDateEnabled"];
          questions.pastDateEnabled = childobj["pastDateEnabled"];
          questions.todayDateEnabled = childobj["todayDateEnabled"];
          if (questions.isQuestionRequired == "Y" || (questions.validations != null && questions.validations.length > 0)) {
            questions["IsValid"] = false;
          }
          else {
            questions["IsValid"] = true;
          }
          if (questions.parentQuesResponse === "") {

            // this.conditionQues.push({ que: questions.questionTextShort, value: questions.parentQuesResponse, isSelected: true });
            questions["isParentQuestionVisible"] = true;
          }
          else {
            //this.conditionQues.push({ que: questions.questionTextShort, value: questions.parentQuesResponse, isSelected: false });
            questions["isParentQuestionVisible"] = false;
          }

          response.questions.push(questions);
        });
        response.isDefaultControl = true;
        response.sequenceNumber = 1;
        debugger;
        response.questions.sort((a, b) => (a.sequenceNumber > b.sequenceNumber) ? 1 : -1);
        //x.response=response;
        gd.response.push(response);
        debugger;

        sections.gridQuestions.push(gd);
      });
      //end grid changes 

      this.surveyDetail.sections.push(sections);

    });

    this.sections = this.surveyDetail.sections.map(function (section) {
      return {
        sectionId: section.sectionId,
        section,
        isProcessed: false
      };
    });

    if (this.sections && this.sections.length > 0) {
      this.currentSection = this.sections[0];

      this.currentSectionIndex = 1;
    }
    this.checkLastSSection();
  }
  updateRatingConfig(evt) {
  }

  private checkLastSSection() {
    if (this.currentSectionIndex >= this.sections.length) {
      this.lastSection = true;
    }
  }

  onSelect(newValue, event) {
    alert('value = ' + newValue);
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss();
  }

  startSurvey() {
    this.surveyStarted = true;
    console.log(this.surveyResponse);
    this.surveyResponse.groupId="08192ee2-ba24-4cbd-2bd7-08d88bd61e85"; 
    this.manageSurvey.InitiateSuurvey(this.surveyResponse).subscribe(
      (result) => {
        this.surveyResponse.surveyResponseID = result;
      });
    this.structureSections(this.surveyDetail);
  }

  processNext() {
    var loop = "continue";
    console.log("final details : ");
    console.log(this.sections);
    this.sections.forEach((parobj, parindex) => {
      if (parobj.section.questions && parobj.section.questions.length > 0) {
        parobj.section.questions.forEach((childobj, childindex) => {
          if (parobj.sectionId && this.currentSection.sectionId === parobj.sectionId && loop === "continue") {

            if (childobj["Valid"] == false) {
              this.AllFieldsValid = false;
              loop = "Break";
            }
            else {
              this.AllFieldsValid = true;
            }
          }

        });
      }
    });
    if (this.AllFieldsValid === false) {
      this.displayFormError = true;
      console.log("inside scroll");
      document.getElementById('scrolled').scrollIntoView({
        behavior: "smooth"
      });
      return;
    }
    /*
    questionResponses: [{questionId: 3803, questionReponse: "Italian cuisine||Indian cuisine"}]
surveyId: "755b477f-d9e6-4065-a37d-6192dba66b74"
surveyResponseID: 808
    */
    if (this.lastSection) {
      this.sections.forEach(section => {
        section.section.questions.forEach(question => {
          // tslint:disable-next-line:one-variable-per-declaration
          const resp: SurveyQuestionResponse = {
            questionId: question.questionId,
            questionReponse: question.defaultResponse
          };
          this.surveyResponse.questionResponses.push(resp);
        });
        //changes for reading grid responses.
        debugger;
        section.section.gridQuestions.forEach((gridobj, indexgrid) => {
          gridobj.response.forEach((responseObj, indexresponse) => {
            debugger;
            if (responseObj["sequenceNumber"] != gridobj.response.length) {
              responseObj.questions.forEach((childobj, indexque) => {

                debugger;
                /*if(childobj["inputType"]=='sign')
                {
                this.IsAnyESignPresent=true;
                }*/
                const resp: SurveyQuestionResponse = {
                  questionId: gridobj.response[0].questions[indexque].questionId,
                  questionReponse: childobj.defaultResponse
                };
                this.surveyResponse.questionResponses.push(resp);

              })
            }
          })
        });



      });
      //submit 
      console.log(this.surveyResponse);
      this.surveyResponse.groupId="08192ee2-ba24-4cbd-2bd7-08d88bd61e85"; 
      this.manageSurvey.SubmitSurvey(this.surveyResponse).subscribe(
        (result) => {
          console.log('success');
          this.toastservice.presentToast('Survey saved successfully');

          this.dismiss();
        });
    }
    else {
      this.currentSection = this.sections[this.currentSectionIndex];
      this.currentSectionIndex++;
      this.displayFormError = false;
      this.checkLastSSection();
    }


  }

  setValue(question: any, value: string) {
    question.defaultResponse = value;
  }

  private SetChoiceData(question: any, value: any) {
    question = value;
  }

  public questionValidityStatus(event){
    if(event.name="questionValidity"){
      this.blockSlide = event.data;
      console.log("block status : "+this.blockSlide);
      if(this.blockSlide == undefined)
        this.blockSlide = false;
    }
  }
}
