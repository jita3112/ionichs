/* Author : Hariom Sinha 557173 */
/* This module will validate Survey Inputs based on Custom Options */
export class Validation{

  /* Current Custom Validations Supported for Surveys */
  /* Email Check, Age Validation, Range Validator, SSN, Demo Validation, Value Range,
     small CAse and Upper Case */
  /* Use User Defined method block only for VAlidating Inputs */
  
  /*  JSON to Store ENd Result */

  result : any = {
    "error": [],
    "validationStatus":""
  };

  ngOnInit(){
    this.result["error"] = [];
    this.result["validationStatus"] = ""
  }


  /*
    errorMessage: "Invalid Email ID."
    excludeCharacters: "!,#,$,%,^,&,*,(,/,;,',<,>,?,",',:,;,{,},[,],\"
    format: "abc@a.com"
    isCharacterAllowed: false
    isNumericAllowed: false
    isSpecialCharacterAllowed: false
    maxRange: null
    minRange: null
    regularExpression: "^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
    validationId: 40
    validationType: "2"
  */
  public validateEmail(userInput, validationJSON){

    var userInput = userInput;
    var regEx = validationJSON.regularExpression;
    var result = userInput.match(regEx);

    if(result == null)
      {
        this.result["error"].push(validationJSON['errorMessage']);
        return false;
      }
    else 
      return true;
     
  }


  /*
    errorMessage: "Incorrect age"
    excludeCharacters: ""
    format: ""
    isCharacterAllowed: false
    isNumericAllowed: false
    isSpecialCharacterAllowed: false
    maxRange: 120
    minRange: 0
    regularExpression: ""
    title: null
    validationId: 41
    validationType: "1"
  */
  validateAge(userInput, validationJSON){

      if(userInput <= validationJSON.maxRange && userInput >= validationJSON.minRange)
        return true;
      else
      {
        this.result["error"].push(validationJSON['errorMessage']);
        return false;
      }
  }

  /*
    errorMessage: "Input should be between 1 to 15."
    excludeCharacters: ""
    format: ""
    isCharacterAllowed: false
    isNumericAllowed: false
    isSpecialCharacterAllowed: false
    maxRange: 15
    minRange: 1
    regularExpression: ""
    title: null
    validationId: 42
    validationType: "1"
  */

  validateRange(userInput, validationJSON){

    if(userInput <= validationJSON.maxRange && userInput >= validationJSON.minRange)
      return true;
    else
    {
      this.result["error"].push(validationJSON['errorMessage']);
      return false;
    }
  }

/*
    errorMessage: "Please enter a valid SSN."
    excludeCharacters: "q,w,e,r,t,y,u,i,o,p,a,s,d,f,g,h,j,k,l,z,c,v,b,n,m,!,@,#,$,%,^,&,*,(,.,/,;,',<,>,?,",',:,;,{"
    format: "11-111-1111"
    isCharacterAllowed: false
    isNumericAllowed: true
    isSpecialCharacterAllowed: true
    maxRange: null
    minRange: null
    regularExpression: "^(?!666|000|9\\d{2})\\d{3}-(?!00)\\d{2}-(?!0{4})\\d{4}$"
    title: null
    validationId: 64
    validationType: "2"
*/

  validateSSN(userInput, validationJSON){
    console.log("SSN");
    var regEx = "^(?!666|000|9\\d{2})\\d{3}-(?!00)\\d{2}-(?!0{4})\\d{4}$";
    var result = userInput.match(regEx);


    if(result == null)
    {
      this.result["error"].push(validationJSON['errorMessage']);
      return false;
    }
    else  
       return true;

  }

/*
    errorMessage: "Only characters are alowed"
    excludeCharacters: "a"
    format: ""
    isCharacterAllowed: true
    isNumericAllowed: false
    isSpecialCharacterAllowed: false
    maxRange: null
    minRange: null
    regularExpression: ""
    title: null
    validationId: 69
    validationType: "2"
*/

  validateDemo(userInput, validationJSON){

    var userInput = userInput;
    var regEx = "^[b-zB-Z][b-zB-Z\s]*$";
    var result = userInput.match(regEx);

    if(result == null)
    {
      this.result["error"].push(validationJSON['errorMessage']);
      return false;
    }
    else  
       return true;

  }

  /*
    errorMessage: "value greater than  3000"
    excludeCharacters: ""
    format: ""
    isCharacterAllowed: false
    isNumericAllowed: false
    isSpecialCharacterAllowed: false
    maxRange: 3000
    minRange: 0
    regularExpression: ""
    title: null
    validationId: 70
    validationType: "1"
  */
 validateValueRange(userInput, validationJSON){

    if(userInput <= validationJSON.maxRange && userInput >= validationJSON.minRange)
       return true;
    else
    {
      this.result["error"].push(validationJSON['errorMessage']);
      return false;
    }
  }


  /*
    description: null
    errorMessage: "Please enter only Upper case letters"
    excludeCharacters: ""
    format: ""
    isCharacterAllowed: true
    isNumericAllowed: false
    isSpecialCharacterAllowed: false
    maxRange: null
    minRange: null
    regularExpression: "[A-Z]*"
    title: null
    validationId: 73
    validationType: "2"
  */

  validateUpperCase(userInput, validationJSON){

    var userInput = userInput;
    var regEx = "[A-Z]+";    
    var result = userInput.match(regEx);
    
    if(result == null)
    {
      this.result["error"].push(validationJSON['errorMessage']);
      return false;
    }
    else  
        return true;

  }



  /*
    errorMessage: "Small case only"
    excludeCharacters: ""
    format: ""
    isCharacterAllowed: true
    isNumericAllowed: false
    isSpecialCharacterAllowed: false
    maxRange: null
    minRange: null
    regularExpression: "[A-Z]*"
    title: null
    validationId: 72
    validationType: "2"
  */

  validateLowerCase(userInput, validationJSON){

  var userInput = userInput;
  var regEx = "[a-z]+";
  var result = userInput.match(regEx);
    
    if(result == null)
    {
      this.result["error"].push(validationJSON['errorMessage']);
      return false;
    }
    else  
        return true;
  }

  /*
    errorMessage: "enter correct date of birth"
    excludeCharacters: ""
    format: "11/11/1990"
    isCharacterAllowed: true
    isNumericAllowed: false
    isSpecialCharacterAllowed: false
    maxRange: null
    minRange: null
    regularExpression: "^(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\\d\\d$"
    title: null
    validationId: 43
    validationType: "2"
  */

 validateDOB(userInput, validationJSON){
  console.log("DOB");
  var userInput = userInput;
  var regEx =  "^(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\\d\\d$";
  var result = userInput.match(regEx);

    if(result == null)
    {
      this.result["error"].push(validationJSON['errorMessage']);
      return false;
    }
    else  
        return true;
  }

  /*
    errorMessage: "Only Numbers are allowed."
    excludeCharacters: "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,!,@,#,$,%,^,&,*,(,.,/,;,',+"
    format: ""
    isCharacterAllowed: false
    isNumericAllowed: true
    isSpecialCharacterAllowed: false
    maxRange: null
    minRange: null
    regularExpression: ""
    title: null
    validationId: 44
    validationType: "2"
  */
  validateNumber(userInput, validationJSON){

    var userInput = userInput;
    var regEx = "([0-9]+)";
    var result = userInput.match(regEx);

      if(result == null)
      {
        this.result["error"].push(validationJSON['errorMessage']);
        return false;
      }
      else  
          return true;
  }


  /*
    description: null
    errorMessage: "Input should be only numbers"
    excludeCharacters: "q,w,e,r,t,y,u,i,o,p,a,s,d,f,g,h,j,k,l,z,x,c,v,b,n,m,!,@,#,$,%,^,&,*,(,.,/,;,'"
    format: "123"
    isCharacterAllowed: false
    isNumericAllowed: true
    isSpecialCharacterAllowed: false
    maxRange: null
    minRange: null
    regularExpression: ""
    title: null
    validationId: 66
    validationType: "2"
  */
 
  validateNumericInput(userInput, validationJSON){

    var userInput = userInput;
    var regEx = "([0-9]+)";
    var result = userInput.match(regEx);
  
      if(result == null)
      {
        this.result["error"].push(validationJSON['errorMessage']);
        return false;
      }
      else  
          return true;
  }



  /*
    description: null
    errorMessage: "Special Characters are not Valid input"
    excludeCharacters: "!,@,#,$,%,^,&,*,(,.,/,;,',<,>,?,",',:,;,{,},[,],\"
    format: "123abc"
    isCharacterAllowed: true
    isNumericAllowed: true
    isSpecialCharacterAllowed: false
    maxRange: null
    minRange: null
    regularExpression: ""
    title: null
    validationId: 67
    validationType: "2"
  */

  validateSpecialCharacter(userInput, validationJSON){

    var userInput = userInput;
    var regEx = "[0-9]+[a-zA-Z]+";
    var result = userInput.match(regEx);

      if(result == null)
      {
        this.result["error"].push(validationJSON['errorMessage']);
        return false;
      }
      else  
          return true;
        
  }

  /*
    errorMessage: "Input should be only characters."
    excludeCharacters: ""
    format: "abc"
    isCharacterAllowed: true
    isNumericAllowed: false
    isSpecialCharacterAllowed: false
    maxRange: null
    minRange: null
    regularExpression: ""
    title: null
    validationId: 68
    validationType: "2"
  */
  validateCharacter(userInput, validationJSON){

    var userInput = userInput;
    var regEx = "^[a-zA-Z][a-zA-Z\s]*$";
    var result = userInput.match(regEx);

      if(result == null)
      {
        this.result["error"].push(validationJSON['errorMessage']);
        return false;
      }
      else  
          return true;
        
  }


  public checkCustomValidationsIfAny(value, question){

    //Iterate through all the Custom Validations and CHeck the Validity 
    var isValid = true;
    console.log(question);
    for( var validation in question.validationDetails){
      console.log("inside loop"+validation);
      console.log(question.validationDetails[validation]["validationId"]);
      //Validation IDs Present to Validate are : 

      var isValidEmail = true; //40
      var isValidAge = true; //41
      var isValidRange = true; //42
      var isValidDOB = true; //43
      var isValidNumber = true; //44
      var isValidSSN = true; //64
      var isValidNumeric = true; //66
      var isValidExcludeSpecialCharacter = true; //67
      var isValidCharacterOnly = true; //68
      var isValidDemo = true; //69
      var isValidValueRange = true; //70
      // var isValidDateRange = true; //71
      var isValidSmallCase = true; //72
      var isValidUpperCase = true; //73

      if(question.validationDetails[validation]['validationId'] === 40){
        isValidEmail =  this.validateEmail(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 41){
        isValidAge = this.validateAge(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 42){
        isValidRange = this.validateRange(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 43){
        isValidDOB = this.validateDOB(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 44){
        isValidNumber =  this.validateNumber(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 64){
        isValidSSN = this.validateSSN(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 66){
        isValidNumeric = this.validateNumericInput(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 67){
        isValidExcludeSpecialCharacter = this.validateSpecialCharacter(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 68){
        isValidCharacterOnly =  this.validateCharacter(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 69){
        isValidDemo =  this.validateDemo(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 70){
        isValidValueRange =  this.validateValueRange(value, question.validationDetails[validation]);
      }
      // if(validation['validationId'] === 71){

      // }
      if(question.validationDetails[validation]['validationId'] === 72){
        isValidSmallCase = this.validateLowerCase(value, question.validationDetails[validation]);
      }
      if(question.validationDetails[validation]['validationId'] === 73){
        isValidUpperCase = this.validateUpperCase(value, question.validationDetails[validation]);
      }


      //check final validation -- if all are true then its correct Or Else Wrong Input
      if(isValidEmail && isValidAge && isValidCharacterOnly && isValidDOB 
        && isValidExcludeSpecialCharacter && isValidNumber && isValidNumeric 
        && isValidRange && isValidSSN && isValidSmallCase && isValidUpperCase 
        && isValidValueRange && isValidSmallCase && isValidDemo)
        {
          this.result["validationStatus"] = true;
        }  
        
      else
         this.result["validationStatus"] = false;
      
    }
    return this.result;
  }

}