import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { finalize, tap } from 'rxjs/operators';
import { Toastservice } from './toast.service';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
    authtoken: string;
    count = 0;
    constructor(private authService: AuthService, private toastCtrl: Toastservice){
        this.authService.authToken.subscribe(
            data => this.authtoken = data
        );
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.toastCtrl.presentLoading();
        this.count++;
        if (this.authtoken !== '') {
            const tokenizedreq = req.clone({
                headers: new HttpHeaders({
                    Authorization: 'Bearer ' + this.authtoken,
                    'Cache-Control': 'no-cache',
                    Pragma: 'no-cache',
                    Expires: 'Sat, 01 Jan 2000 00:00:00 GMT'
                })
            });
            req = tokenizedreq;
        }
        return next.handle(req).pipe(
            finalize(() => {
                this.count--;
                if (this.count === 0) {
                    this.toastCtrl.dismissLoading();
                }
            })
        )
    }


}
