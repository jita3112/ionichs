import { Injectable, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { R3DelegatedFnOrClassMetadata } from '@angular/compiler/src/render3/r3_factory';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  private isUserloggedIn = false;
  constructor(private router: Router, private authService: AuthService) {
    this.authService.userAuthStatus.subscribe(loggedInStatus => this.isUserloggedIn = loggedInStatus);
   }

  canActivate(): boolean {
    console.log('Activate Guard:' + this.isUserloggedIn);
    if (this.isUserloggedIn) {
      return true;
    }
    else {
      this.router.navigateByUrl('login');
      return false;
    }
  }
}
