import { Injectable } from '@angular/core'; 
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service'; 
import { Toastservice } from './toast.service';  

@Injectable({
  providedIn: 'root'
})
export class GeoLoactionService {
   
  constructor(private httpservice: HttpService, private toastCtrl: Toastservice) {
  }
   
   getLocation(longitude,latitude):Observable<any> {
  return  this.httpservice.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + '%2C' + longitude+ '&language=en')
   /* .subscribe(
      (location) => {
        return location;
      },
        (error )=> {
          this.toastCtrl.presentToast('Error in Location retreive :'
          +error);
        }

      );*/
  } 
}
