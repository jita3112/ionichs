import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpService } from './http.service';
import { LoginModel } from '../models/login.model';
import { Constants } from './../constants';
import {tap, catchError} from 'rxjs/operators';
import { Toastservice } from './toast.service';
import { RegistrationModel } from '../models/registration.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public userAuthStatus = new BehaviorSubject<boolean>(false);
  public authToken = new BehaviorSubject<string>('');
  constant: Constants = new Constants();

  constructor(private router: Router, private httpService: HttpService, private toastCtrl: Toastservice) { }

  login(loginModel: LoginModel): Observable<any> {
    return this.httpService.post(this.constant.postlogin, loginModel).pipe(
      tap(
        (result) => {
          this.userAuthStatus.next(true);
          this.authToken.next(result['auth_token']);
          localStorage.setItem('auth_token', result['auth_token'] );
        }
      ),
      catchError( (err) => {
        this.authToken.next('');
        localStorage.removeItem('auth_token');
        return throwError(err);
      })
    );
  }
  register(registrationModel: RegistrationModel): Observable<any> {
    return this.httpService.post(this.constant.postregister, registrationModel).pipe(
      tap(
        (result) => {
          this.authToken.next('');
          localStorage.removeItem('auth_token');
        }
      ),
      catchError( (err) => {
        this.authToken.next('');
        localStorage.removeItem('auth_token');
        return throwError(err);
      })
    );
  }
  logout() {
    this.userAuthStatus.next(false);
    this.authToken.next('');
    localStorage.removeItem('auth_token');
    this.toastCtrl.presentToast('User Logged Out Successfully');
    this.router.navigate(['login']);
  }
}
