import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class Toastservice {
    constructor(private toastCtrl: ToastController, public loadingController: LoadingController){}

    loader: any;
    async presentToast(toastMessage: string, color: string = 'primary') {
        const toastPromise = this.toastCtrl.create({
            message: toastMessage,
            duration: 5000,
            position: 'bottom',
            color
          });

        (await toastPromise).present();
    }

    async presentLoading() {
        if (this.loader === undefined) {
            this.loader = await this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...'
              });
            await this.loader.present();
        }
    }

    async dismissLoading() {
        if (this.loader !== undefined) {
            this.loader.dismiss();
            this.loader = undefined;
        }
    }
}
