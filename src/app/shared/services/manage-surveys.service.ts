import { Injectable } from '@angular/core';
import { CardModel } from '../models/card.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';
import { Constants } from '../constants';
import { Toastservice } from './toast.service';
import { SurveyDetail } from '../models/survey.model';
import { map, tap, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ManageSurveysService {
  cardsSubject = new BehaviorSubject<CardModel[]>([]);

  constructor(private httpservice: HttpService, private toastCtrl: Toastservice, private http: HttpClient) {
  }
  constant: Constants = new Constants();
  getAllServeys() {
    this.httpservice.get(this.constant.getUserDashboard).subscribe(
      (results) => {
        this.cardsSubject.next(results);
      });
  }

  // deleteSurvey(id): Observable<any> {
  //   return this.httpservice.delete(this.constant.getSurveyById, id);
  // }

  toggleFavourite(id) {
    // this.cards.find(p => p.id === id).isFavourite = !this.cards.find(p => p.id === id).isFavourite;
  }

  surveyResults(surveyId: string): Observable<any> {
    return this.httpservice
      .getById(this.constant.getSurveyById, surveyId);
  }
  GetSurveyDetails(surveyId: string, mockResponse: boolean = false): Observable<any> {
    if (mockResponse) {
      return this.http.get('./../../assets/json-data/survey-view.json');
    }
    else {
      return this.httpservice
      .getById(this.constant.getSurveyById, surveyId)
      .pipe(map(result => {
        if (result != null) {
          this.getSurveyDetails(result);
        }
        return result;
      }));
    }
  }
  DeleteSrvey(surveyId: string) {
    this.httpservice
      .delete(this.constant.getSurveyById, surveyId)
      .subscribe(result => {
        this.toastCtrl.presentToast('Survey Deleted!');
        this.getAllServeys();
      },
        error => {
          this.toastCtrl.presentToast('Survey Delete Failed!', 'danger');
        });
  }
  ArchiveSurvey(surveyId: string) {
    this.httpservice
      .post(this.constant.archiveSurvey + '/' + surveyId, null)
      .subscribe(result => {
        this.toastCtrl.presentToast('Survey Archived!');
        this.getAllServeys();
      },
        error => {
          this.toastCtrl.presentToast('Survey Archive Failed!', 'danger');
        });
  }
  InitiateSuurvey(payload: any){
    return this.httpservice
      .post(this.constant.initiateSurveyResponse, payload)
      .pipe(
        tap((result: any) => console.log('initiaate response: ' + result)),
        catchError(error => this.toastCtrl.presentToast('Initiatte Survey Failed!', 'danger'))
      );
  }

  SubmitSurvey(payload: any){
    return this.httpservice
      .post(this.constant.SubmitSurveyResponse, payload)
      .pipe(
        tap((result: any) => console.log('submitted survey: ' + result)),
        catchError(error => this.toastCtrl.presentToast('Submit Survey Failed!', 'danger'))
      );
  }

  private getSurveyDetails(queslist: SurveyDetail): SurveyDetail {

    if (Object.keys(queslist).length !== 0) {
      if (queslist.sections !== null) {
        queslist.sections = queslist.sections.sort((a, b) => (a.sequenceNumber > b.sequenceNumber) ? 1 : -1);
      }
      if (queslist.sections !== null) {
        queslist.sections.forEach((obj, index) => {
          if (obj.questions !== null) {
            obj.questions = obj.questions.sort((a, b) => (a.sequenceNumber > b.sequenceNumber) ? 1 : -1);
          }
        });
      }
    }
    return queslist;
  }
}
