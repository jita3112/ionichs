import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';
import { LoginModel } from '../shared/models/login.model';
import { Toastservice } from '../shared/services/toast.service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { ModalController, Platform } from '@ionic/angular';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email:string; 
  password:string;
  name:string;
  dataBase:any;
  constructor(private authService: AuthService, 
              private router: Router, 
              private toastCtrl: Toastservice,
              private sqlite: SQLite,
              private platform: Platform,
              private modalController:ModalController,
              private faio:FingerprintAIO) { 
                
              }

  UseBiometric(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      this.dataBase=db;
        this.dataBase.executeSql('SELECT count(1) AS mycount FROM sqlite_master WHERE type="table" AND name="UserDetails"',[]).then(result=>{
         
          let count=0;
          count= result.rows.item(0).mycount;
          if (count>0)
          {
             this.faio.show({
            //  clientId: 'Fingerprint-Demo',
            // clientsecret:'password',
            // localizedFallbackTitle: 'Use Pin',
            // localizedReason: 'Please authenticate'
            }).then(()=>{
              this.dataBase.executeSql('SELECT  username,password FROM UserDetails',[]).then(result=>{
                      
                      const login = new LoginModel();
                      login.username = result.rows.item(0).username;
                      login.password = result.rows.item(0).password;
                      this.authService.login(login).subscribe(
                        result => {
                          if (result !== null) {
                            this.toastCtrl.presentToast('Login Successful');
                            this.router.navigate(['dashboard']);
                          }});
              });
            });
          }
      });
    });
  }
  ngOnInit() {
     this.UseBiometric();
  }
  CheckifUserDetailsPresentinLocalDB(){
        
         this.sqlite.create({
          name: 'data.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          this.dataBase=db;
          
            db.executeSql('SELECT count(1) AS mycount FROM sqlite_master WHERE type="table" AND name="UserDetails"',[]).then(result=>{
         
            let count=0;
            count= result.rows.item(0).mycount;
            if (count<=0)
            {
              this.showConfirmationBox();
            }
            else {
              this.router.navigate(['dashboard']);
            }
          })

        });
      }
        
        errorCB(err):void {
          this.toastCtrl.presentToast("Error processing SQL: "+err);
        }
 
    onSignUp()
    {
      this.router.navigate(['registration']);
      
    }
    async showConfirmationBox(){

      const modal = await this.modalController.create({
        component: ConfirmDialogComponent,
        cssClass: 'my-custom-class',
      
          componentProps: {
            'email': this.email,
            'password': this.password
           
          }
        
      });
      return await modal.present();
    }
      OnLogin() {
        //Pass Login form here.
        const login = new LoginModel();
        login.username = this.email;
        login.password = this.password;
        this.authService.login(login).subscribe(
          result => {
            if (result !== null) {
              this.toastCtrl.presentToast('Login Successful');
                //logic to to show confirmation popup;
                this.CheckifUserDetailsPresentinLocalDB();

            //  this.router.navigate(['dashboard',{email:this.email}]);
            }
          },
          error => {
            this.toastCtrl.presentToast('Login Failed');
          });
      }

}
