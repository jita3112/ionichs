import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ModalController, ToastController } from '@ionic/angular';
import { CardModel } from '../shared/models/card.model';
import { ManageSurveysService } from '../shared/services/manage-surveys.service';
import { SurveyPage } from './../shared/survey/survey.page';
import { Toastservice } from '../shared/services/toast.service';
import { SurveyDetail } from '../shared/models/survey.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  selectedsegment: string = 'all';
  constructor(
    private actionSheetController: ActionSheetController,
    private manageService: ManageSurveysService,
    private modalController: ModalController,
    private toastCtrl: Toastservice) { }

  cards: CardModel[];
  filter:string;
  ngOnInit() {
    this.manageService.getAllServeys();
    this.manageService.cardsSubject.subscribe(data => this.cards = data);
  }

  segmentChanged(event: any) {
    //invoked when segment selected
    //perform page switch based on this.
    console.log('segment seleted' + event.target.value);
    this.selectedsegment = event.target.value;
  }

  toggleFavourite(id: string) {
    const card = this.cards.find(p => p.surveyId === id);
    card.isFavourite = !card.isFavourite;
    console.log(card.isFavourite);
  }

  async presentActionSheet(id: string) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Actions',
      cssClass: 'my-custom-class',
      buttons: [
        {
          text: 'Take Survey',
          icon: 'add-circle',
          handler: () => {
            this.manageService.GetSurveyDetails(id, false).subscribe(
              result => {
                this.presentModal(result);
              },
              error => {
                this.toastCtrl.presentToast('Failed to fetch Survey Details', 'danger');
              });
            console.log('Share clicked');
          }
        },

      //Disabling the below Btns as per CR
      //   {
      //   text: 'Results',
      //   icon: 'share',
      //   handler: () => {
      //     console.log('Play clicked');
      //   }
      // }, {
      //   text: 'Archive',
      //   icon: 'archive',
      //   handler: () => {
      //     this.manageService.ArchiveSurvey(id);
      //   }
      // },
      // {
      //   text: 'Delete',
      //   role: 'destructive',
      //   icon: 'trash',
      //   handler: () => {
      //     this.manageService.DeleteSrvey(id);
      //   }
      // },
       {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  getSurveys() {
    switch (this.selectedsegment) {
      case 'all':
      if (this.filter === undefined || this.filter === '') {
        return this.cards;
      }
      return this.cards.filter( p => p.name?.toLowerCase().indexOf(this.filter.toLowerCase()) > -1
      || p.surveyGroupName?.toLowerCase().indexOf(this.filter.toLowerCase()) > -1);
      break;
      case 'favourites':
      if (this.filter === undefined || this.filter === '') {
        return this.cards.filter(p => p.isFavourite === true);
      }
      return this.cards.filter(p => p.name?.toLowerCase().indexOf(this.filter.toLowerCase()) > -1
      || p.surveyGroupName?.toLowerCase().indexOf(this.filter.toLowerCase()) > -1).filter(p => p.isFavourite === true);
      break;
    }
  }

  filterSurveys(event: any) {
    if (event === undefined || event.target.value === undefined || event.target.value === '')
    {
      this.filter = '';
    } else {
      this.filter = event.target.value;
    }
  }

  async presentModal(surveyDetail: SurveyDetail) {
    const modal = await this.modalController.create({
      component: SurveyPage,
      cssClass: 'my-custom-class',
      componentProps: {
        'surveyDetail': surveyDetail
      }
    });
    return await modal.present();
  }
}

