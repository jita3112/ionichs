import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HttpService} from './shared/services/http.service'
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './shared/services/auth.service';
import { Toastservice } from './shared/services/toast.service';
import {  GeoLoactionService} from './shared/services/geocode.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  
  public logginPages = [
    {
      title: 'Login',
      url: '/folder/Login',
      icon: 'mail'
    },
    {
    title: 'Sign Up',
    url: '/registration',
    icon: 'paper-plane'
  }
  ];

  public appPages = [
    {
      title: 'Dashboard',
      url: '/dashboard',
      icon: 'mail'
    }

    //Disabling the below Btns as per CR
    // {
    //   title: 'Validation Settings',
    //   url: '/folder/Outbox',
    //   icon: 'paper-plane'
    // },
    // {
    //   title: 'Organization and Groups',
    //   url: '/folder/OrganizationandGroups',
    //   icon: 'heart'
    // },
    // {
    //   title: 'Manage Groups',
    //   url: '/folder/ManageGroups',
    //   icon: 'heart'
    // },
    // {
    //   title: 'Users',
    //   url: '/folder/Users',
    //   icon: 'heart'
    // }
  ];
  WelcomeNote: string;
  isUserLoggedIn: boolean;
    latitude:number;
    longitude :number;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private toastservice:Toastservice,
    private  geoLoactionService:GeoLoactionService,
    private httpservice: HttpService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    if (localStorage.getItem('auth_token') !== null) {
      this.authService.authToken.next(localStorage.getItem('auth_token'));
      this.authService.userAuthStatus.next(true);
    }
    this.authService.userAuthStatus.subscribe(status =>{ this.isUserLoggedIn = status
      this.WelcomeNote = this.isUserLoggedIn ? 'Hello User!' : 'Please Login to Continue';
    });
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
    this.WelcomeNote = this.isUserLoggedIn ? 'Hello User!' : 'Please Login to Continue';
     // Options: throw an error if no update is received every 30 seconds.
      //
      
// as object method
var obj = {bar: this.onSuccess  };
      var watchID = navigator.geolocation.watchPosition(obj.bar , this.onError, { timeout: 30000 });
     console.log(watchID);


  }
//onSuccess Callback
  //   This method accepts a `Position` object, which contains
  //   the current GPS coordinates
  //
  onSuccess(position) {
    var element = document.getElementById('geolocation');
    element.innerHTML = '<ion-row >'+
                        '<ion-col class="center">'+
                        '<ion-note style="color:white;">Latitude: '  + position.coords.latitude      + '</ion-note></ion-col></ion-row><ion-row><ion-col>' +
                        '<ion-note style="color:white;">Longitude: ' + position.coords.longitude     + '</ion-note></ion-col></ion-row>' ;
                         
                  // this.SetGeocity(position.coords.latitude ,position.coords.longitude)
    
    }
   
    SetGeocity(latitude,longitude)
    { 
      var element = document.getElementById('geolocation');
      this.geoLoactionService.getLocation( longitude , latitude).subscribe(
        result => {
          if (result !== null) {
            element.innerHTML = element.innerHTML+result ;
          }
        },
        error => {
          this.toastservice.presentToast('Geo location service Failed to locate city ');
        });
    }
    displayLocation(latitude,longitude){
      var request = new XMLHttpRequest();

      var method = 'GET';
      var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&sensor=true';
      var async = true;

      request.open(method, url, async);
      request.onreadystatechange = function(){
        if(request.readyState == 4 && request.status == 200){
          var data = JSON.parse(request.responseText);
          var address = data.results[0];
          document.write(address.formatted_address);
        }
      };
      request.send();
    };

   // onError Callback receives a PositionError object
   //
    onError(error) {
      if ( this.toastservice !== null)
      {
   this.toastservice.presentToast ('code: '    + error.code    + '\n' +
           'message: ' + error.message + '\n');
      }
    }
 
  // // temporary function. thin needs to be handled in rouutinng AuthGurad
  // redirectToLogin() {
  //   this.router.navigateByUrl('folder/Login');
  // }

  Logout() {
    this.authService.logout();
    this.selectedIndex = 0;
  }
}
