import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';
import { RegistrationModel } from '../shared/models/registration.model';
import { Toastservice } from '../shared/services/toast.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  fname:string; 
  lname:string; 
  password:string;
  username:string;
  email:string;
  constructor(private authService: AuthService, private router: Router, private toastCtrl: Toastservice) { }

  ngOnInit() {
  }

  SignUp() {
    //Pass Login form here.
    const register = new RegistrationModel();
    register.email= this.email;
    register.password = this.password;
    register.firstName = this.fname;
    register.lastName = this.lname;
    register.location = null;
    register.organizationId = null;

    this.authService.register(register).subscribe(
      result => {
        if (result !== null) {
          this.toastCtrl.presentToast('Registration Successful. Please Login.');
          this.router.navigate(['/login']);
        }
      },
      error => {
        this.toastCtrl.presentToast('Registration Failed');
      });
  }

}

